package com.example.task06;

public class Task06Main {
  public static void main(String[] args) {
    // здесь вы можете вручную протестировать ваше решение, вызывая реализуемый
    // метод и смотря результат
    // например вот так:
    new Task06Main().printMethodName();
  }

  void printMethodName() {
    StackWalker walker = StackWalker.getInstance();
    String method = walker.walk(s -> s.map(StackWalker.StackFrame::getMethodName).skip(1).findFirst()).get();
    System.out.println(method);
  }
}
